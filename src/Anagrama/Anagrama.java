package Anagrama;

import java.util.Arrays;
import java.util.Scanner;

public class Anagrama {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("ingresa a");
        String wordA  = scanner.nextLine();

        System.out.println("ingresa b");
        String wordB  = scanner.nextLine();

        wordA = sort(wordA.replaceAll("(\\W)+|(\\d)+",""));
        wordB = sort(wordB.replaceAll("(\\W)+|(\\d)+",""));

        System.out.print(((wordA.equals(wordB))?"Si":"No")+" son anagramas");
    }

    private static String sort(String cad){
        char[] chars = cad.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
}