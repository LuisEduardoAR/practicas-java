package Pocker;

public class Deck {
    public static int countPosDeck = 0;
    private static int n=52;
    private static Card[] cards = new Card[n];
    static Integer[] numTemp = new Integer[n];

    public static void mixCards(){
        Card[] cardsTemp = new Card[n];

        int pos = 0;
        for (cardType cardType :cardType.values()) {
            for (int cardValue = 1;cardValue<=13;cardValue++){
                cardsTemp[pos++]=(new Card(cardValue, cardType));
            }
        }
        int cont = 0;
        while (true){
            int numRand=(int)(Math.random()*52)+1;
            if(unic(numRand)){
                numTemp[cont++] = numRand;
            }
            if(numTemp[n-1]!=null)break;
        }
        cont=0;
        for (Integer ubi:numTemp) {
            cards[cont++] = cardsTemp[ubi-1];
        }                         /*
        for (Card c:cards) {
            System.out.print(c+" ");
        }
        System.out.println();   //*/
    }

    static boolean unic(int num){
        for (Integer numtemp:numTemp) {
            if(numtemp==null){
                return true;
            }else if(numtemp==num){
                return false;
            }
        }
        return true;
    }

    static Card getCard(){
        return cards[countPosDeck++];
    }

}
